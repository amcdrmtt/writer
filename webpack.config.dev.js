const webpack = require('webpack')
const webpackTargetElectronRenderer = require('webpack-target-electron-renderer')

const config = { 
  entry: [ 
    'webpack-hot-middleware/client?reload=true&path=http://localhost:9000/__webpack_hmr', 
    './src/entry.js', 
  ], 
  module: { 
    loaders: [{ 
      test: /\.js?$/, 
      loaders: ['babel-loader'], 
      exclude: /node_modules/ 
    }, { 
      test: /\.css$/, 
      loader: 'style!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader' 
    }, { 
      test: /\.png|\.svg$/, loaders: ['file-loader'] 
    }] 
  }, 
  output: { 
    path: __dirname + '/build', 
    publicPath: 'http://localhost:9000/build/', 
    filename: 'bundle.js' 
  }, 
  resolve: { 
    extensions: ['', '.js'], 
  },
  plugins: [ 
    new webpack.HotModuleReplacementPlugin()
  ] 
}
  
config.target = webpackTargetElectronRenderer(config)
module.exports = config