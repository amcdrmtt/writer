export const addFile = (name) => {
  return {
    type: 'ADD_FILE',
    payload: {
      name
    }
  }
}