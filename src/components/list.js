import React from 'react'

export default ({files, onAddFile}) => {
  const handleKeys = e => {
    if (e.which === 13) {
      onAddFile(e.target.value)
    } 
  }

  return (
    <div>
      <p>{files}</p>
      <input type="text" placeholder="Add file" onKeyDown={handleKeys} />
    </div>
  )
}