import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import writerApp from './reducers'
import App from './components/app'

const store = createStore(writerApp)

render(
  <Provider store={store}>
    <App/>
  </Provider>, 
  document.getElementById('root'))