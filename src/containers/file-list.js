import {connect} from 'react-redux'
import {addFile} from '../actions/files'
import List from '../components/list'

const stateToProps = ({files}) => {
  return {
    files
  }
}

const dispatchToProps = dispatch => {
  return {
    onAddFile: name => {
      dispatch(addFile(name))
    }
  }
}

export default FileList = connect(
  stateToProps,
  dispatchToProps
)(List)