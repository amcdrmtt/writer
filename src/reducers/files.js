export default (state = 'Nothing', {type, payload}) => {
  switch(type) {
    case 'ADD_FILE':
      return `New file: ${payload.name}`
    default:
      return state
  }
}